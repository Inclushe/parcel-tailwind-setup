// Parcel caches this file heavily, so instead we embed it in package.json.

// This file is used to enable Tailwind IDE extensions, like this one:
// https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss

module.exports = require('./package.json')['postcss']['plugins']['tailwindcss']